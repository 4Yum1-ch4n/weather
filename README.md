# Weather
A small application which will show you the current weather in the provided city.

## Project setup
1. Download and extract files.
2. Navigate to the extracted folder via command line.
3. Run `npm install`
4. Run `npm run serve`
5. Open web-browser and head to `https://localhost:8080` (by default)

### Purpose
This project is for educational / testing purposes.
